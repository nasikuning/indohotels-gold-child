<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<!-- header -->
	<header id="homeSliderCarousel" class="carousel slide header-home clear" role="banner">
		<!-- nav -->
		<nav id="mainNav" class="navbar navbar-custom navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
          <button id="nav-menu-mobile" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"  aria-expanded="false">
            <div id="navbar-hamburger">
              <span class="sr-only">Toggle navigation</span> Menu
              <i class="fa fa-bars"></i>
            </div>
            <div id="navbar-close" class="hidden">
              <span class="glyphicon glyphicon-remove"></span>
            </div>
          </button>
				</div><!-- end .navbar-header -->

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php karisma_nav(); ?>
				</div>
			</div><!-- end .container -->
		</nav>
		<!-- /nav -->

		<div class="logo-box">
			<?php krs_headlogo(); ?>
		</div>

		<!-- Wrapper for Slides -->
		<div class="carousel-inner">
			<?php
			if ( function_exists( 'ot_get_option' ) ) {
				$images = explode( ',', ot_get_option( 'krs_gallery', '' ) );
				if ( ! empty( $images ) ) {
					$i=0;
					foreach( $images as $id ) {
						if ( ! empty( $id ) ) {
							if($i++ == 0) {
								$active = 'active';
							} else {$active = '';}
							$full_img_src = wp_get_attachment_image_src( $id, 'gallery-slide-main' );
							$thumb_img_src = wp_get_attachment_image_src( $id, 'gallery-slide-main' );
							?>
							<div class="item <?php echo $active; ?>">
								<div class="home-slider" style="background-image:url('<?php echo $thumb_img_src[0]; ?>');"></div>
							</div>
							<?php
						}
					}
				}
			}
			?>

			<a class="left carousel-control" href="#homeSliderCarousel" data-slide="prev"><span class="icon-prev"></span></a>
			<a class="right carousel-control" href="#homeSliderCarousel" data-slide="next"><span class="icon-next"></span></a>

		</div>

		<section class="booking-section">
			<div class="booking-box">
				<?php do_shortcode("[booking_engine]"); ?>
			</div>
		</section>
	</header>
		<!-- /header -->
