## 08 August 2018 - v1.2.0 ##
* remove thumbnail gallery

## 08 August 2018 - v1.1.9 ##
* add gallery

## 28 July 2018 - v1.1.8 ##
* add room template option

## 27 July 2018 - v1.1.7 ##
* add news with image

## 27 July 2018 - v1.1.6 ##
* add news with image

## 11 April 2018 - v1.0.1 ##
* Fix child theme source

## 1 March 2018 - v1.0.0 ##
* Initial Release
