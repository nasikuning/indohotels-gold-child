<?php

/* Template Name: Rooms - Alpha Template */

get_header('image');

?>

<main role="main">
	<div class="sectionctr content__room-two">
		<div class="container">

			<h1 class="title text-center"><?php the_title(); ?></h1>

			<div class="outer-content">
				<div class="row">
					<?php
						$args = array('post_type'=>'rooms');
						query_posts($args);
						if (have_posts()): while (have_posts()) : the_post();
					?>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="inner-content">
							<a href="<?php the_permalink(); ?>">
								<div class="overlay">
									<div class="rectangel">
										<p><i class="fas fa-eye"></i></p>
									</div>
								</div>
							</a>
							<div class="content-img">
								<?php if ( has_post_thumbnail()) : ?>
									<?php the_post_thumbnail(array(300,150)); ?>
								<?php endif; ?>
							</div>
							<div class="content-desc">
								<div class="content-desctittle">
									<h4><?php the_title(); ?></h4>
								</div>
							</div>
						</div>
					</div>
					<?php endwhile; ?>
					<?php else: ?>
						<article>
							<h2><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h2>
						</article>
					<?php endif; ?>

					<?php get_template_part('pagination'); ?>
				</div>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>
