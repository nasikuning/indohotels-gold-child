<?php
/* Template Name: Home Page Template */ get_header('home'); ?>
<main role="main">

	<?php top_deals(); ?>

	<!-- section -->
  <section id="welcome-hotel">
    <div class="container">
      <div class="box-bg">
        <h2><?php echo ot_get_option('krs_ros_in_title');?></h2>
        <span class="line"></span>
        <?php echo ot_get_option('krs_ros_in'); ?>
      </div>
    </div>
  </section>
	<!-- /section -->

	<?php if (ot_get_option('krs_room_actived') != 'off') : ?>
	<section id="box-feature" style="background-image: url(<?php echo ot_get_option('krs_background_text1'); ?>);">
		<div class="container">

			<?php
			 $args = array(
				'post_type'=> ot_get_option('krs_section_2'),
				'posts_per_page' => 8,
			);

			$krs_query = new WP_Query( $args );
			$count = $krs_query->post_count;
			if(($count == 2) || ($count == 4)) {
				$col = 'col-sm-6 col-md-6';
			} else {
				$col = 'col-md-4';
			}

			if ($krs_query->have_posts()): ?>

			<div class="box-bg" <?php if($col == 'col-md-4') { echo 'style="width: 100%; margin: 0 auto";'; } ?>>
				<div class="heading-box">
					<h2><?php echo ot_get_option('krs_headline_section2');?></h2>
				</div>

				<div class="row">
					<?php while ($krs_query->have_posts()) : $krs_query->the_post(); ?>
					<div class="<?php echo $col ?>">
						<div class="box-room">
							<div class="box-thumb">
								<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php the_post_thumbnail('large'); // Declare pixel size you need inside the array ?>
										<h4><?php the_title(); ?></h4>
								</a>
								<?php endif; ?>
							</div>

						</div>
					</div>
					<?php endwhile; ?>
				</div>
					<?php endif; ?>
			</div> <!-- /box-bg -->
		</div><!-- end .container -->
	</section>
	<?php endif; ?>

	<!-- gallery -->
	<section class="box-home-carousel">
		<div class="heading-box">
			<h2>Our Facilities</h2>
		</div>
		<div class="home-carousel owl-carousel home-gallery">

			<?php
			$args = array(
				'post_type' => 'gallery',
				'phototype'  => 'home',
				);
			query_posts($args);
			if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="item thumb">
				<div class="thumbnails">
					<!-- post thumbnail -->
					<?php if ( has_post_thumbnail()) : //Check if thumbnail exists ?>
						<?php the_post_thumbnail('gallery-slide'); // Declare pixel size you need inside the array ?>
					<?php endif; ?>
					<!-- /post thumbnail -->
				</div>
				<!-- post title -->
				<div class="box-gallery-home">
					<div class="title-gallery-home">
						<h2><?php echo get_the_title(); ?></h2>
						<span class="line-color"></span>
							<?php if (!empty(rwmb_meta('gallery_openning'))) : ?>
							<div class="gallery-time"><?php e('Opening hours', karisma_text_domain); ?>	: <span><?php echo rwmb_meta('gallery_openning'); ?></span> - <span><?php echo rwmb_meta('gallery_closing'); ?></span>
							</div>
							<?php endif; ?>
							<?php if (!empty(rwmb_meta('gallery_telephone'))) : ?>
							<div class="gallery-telephone">
								<div><?php e('Phone', karisma_text_domain); ?>		: <span><?php echo rwmb_meta('gallery_telephone'); ?></span></div>
							</div>
							<?php endif; ?>
					</div>
				</div>
			</div>
			<!-- /post title -->
			<?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>
		</div>
	</section>
	<!-- end gallery -->

	<div class="home-testimonial">
		<div class="heading-box">
			<h2>Testimonials</h2>
		</div>

		<?php footer_slide(); ?>
	</div>

</main>
<?php get_footer(); ?>
