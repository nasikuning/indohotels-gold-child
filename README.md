=== Indohotels Gold Child ===
Contributors: Amri Karisma
Donate link: https://www.amrikarisma.com/
Tags: simple, seo
Requires at least: 4.8
Stable tag: v1.2.0
Version: 1.2.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html


== Description ==
Indohotels Gold Child is a child theme of Indohotelswp


== Installation ==

This section describes how to install the child theme and get it working.

1. You need to install and activate the parent theme first, indohotelswp
2. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
3. Click on the 'Activate' button to use your new theme right away.
4. Navigate to Appearance > Customize in your admin panel and customize to taste.


== Changelog ==
= 1.2.0 =
* remove thumbnail gallery

= 1.1.9 =
* add new gallery

= 1.1.8 =
* add room template option

= 1.1.7 =
* add news with image

= 1.1.6 =
* add news with image

= 1.0.1 =
* Fix child theme source

= 1.0.0 =
* Initial Release
