<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
		<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<!-- header -->
	<header class="header-image-page plain clear" role="banner">
		<!-- nav -->
		<nav id="mainNav" class="navbar navbar-custom">
			<div class="navbar-brand"><?php //krs_headlogo(); ?></div>
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
					</button>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php karisma_nav(); ?>
				</div>
			</div>
		</nav>
		<!-- /nav -->

	</header>
	<!-- /header -->
