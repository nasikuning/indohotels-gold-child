<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<?php wp_head(); ?>

	<script type="text/javascript">
		jQuery(window).scroll(function(){
			if (jQuery(this).scrollTop() > 80) {
				jQuery('#mainNav').addClass('navbar-fixed-top');
			} else {
				jQuery('#mainNav').removeClass('navbar-fixed-top');
			}
		});
	</script>

</head>
<body <?php body_class(); ?>>
<!-- header -->
<header class="header-image-page clear" <?php krs_header_cover(); ?> role="banner">
	<!-- nav -->
	<nav id="mainNav" class="navbar navbar-custom navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
        <button id="nav-menu-mobile" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <div id="navbar-hamburger">
              <span class="sr-only">Toggle navigation</span> Menu
              <i class="fa fa-bars"></i>
          </div>
          <div id="navbar-close" class="hidden">
              <span class="glyphicon glyphicon-remove"></span>
          </div>
        </button>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<?php karisma_nav(); ?>
			</div>

		</div>
	</nav>
	<!-- /nav -->
	<div class="logo-box">
		<?php krs_headlogo(); ?>
	</div>
</header>
<!-- /header -->
