<?php get_header('image'); ?>

<main role="main" class="col-md-12">
<div class="container">
	<!-- section -->
	<section class="box-content">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
				<div class="box-book-rooms"><!-- box booking details -->
					<div class="room-details">
						<div class="col-sm-12 col-md-12">
							<div class="room-booking">
								<div class="room-box">
									<div class="room-title-box text-center">
										<h2 class="room-title"><?php the_title(); ?></h2>
									</div>
									<!-- Slider -->
									<div class="span12" id="slider">
										<!-- Top part of the slider -->
										<div class="span8" id="carousel-bounding-box">
											<div class="carousel slide" id="myCarousel">
												<!-- Carousel items -->
												<div class="carousel-inner">
													<?php
													$images = rwmb_meta( 'indohotels_imgadv', 'size=gallery-slide' );
													if ( !empty( $images ) ) {
														$i = 0;
														foreach ( $images as $image ) {
															if($i++ == 0) {
																$active = 'active';
															} else {$active = '';}
															echo '<div id="slide-'. $i .'" class="'. $active .' item">';

															echo '<img src="', esc_url( $image['url'] ), '"  alt="', esc_attr( $image['alt'] ), '">';
															echo '<a class="carousel-control left" data-slide="prev" href="#myCarousel"><span><i class="fa fa-chevron-left" aria-hidden="true"></i></span></a> <a class="carousel-control right" data-slide="next" href="#myCarousel"><span><i class="fa fa-chevron-right" aria-hidden="true"></i></span></a>';
															echo '</div>';
														}


													}
													?>
												</div>
												<?php  ?>

												<?php  ?>
											</div>
										</div>
									</div><!--/Slider-->

									<div class="room-details-spec">
										<div class="row">
											<div class="col-md-3 col-sm-3">
												<span class="room-title">Room Size</span>
												<span class="room-value"><?php echo rwmb_meta( 'room_size' ); ?> m2</span>
											</div><!-- end .col-md-3 -->
											<div class="col-md-3 col-sm-3">
												<span class="room-title">View </span>
												<span class="room-value"><?php echo rwmb_meta( 'room_view' ); ?></span>
											</div><!-- end .col-md-3 -->
											<div class="col-md-3 col-sm-3">
												<span class="room-title">Ocupancy</span>
												<span class="room-value"><?php echo rwmb_meta( 'room_occupancy' ); ?> Person</span>
											</div><!-- end .col-md-3 -->
											<div class="col-md-3 col-sm-3">
												<span class="room-title">Bed Size</span>
												<span class="room-value"><?php echo rwmb_meta( 'bed_size' ); ?></span>
											</div><!-- end .col-md-3 -->
										</div><!-- end .row -->
									</div><!-- end .room-details-spec -->
								</div><!-- end .room-box -->

								<div class="room-details-desc">
									<?php the_content(); ?>
								</div>
							<?php
							$data['propery_id'] = get_option('idn_booking_engine.propery_id');
							?>
							<div class="text-center">
								<a href="//www.indohotels.id/website/property/<?php echo $data['propery_id']; ?>" class="btn btn-check"><?php _e('Check Availability', karisma_text_domain); ?>
								</a>
							</div>
						</div>

					</article>
					<!-- /article -->

				<?php endwhile; ?>

			<?php else: ?>

				<!-- article -->
				<article>

					<h1><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h1>

				</article>
				<!-- /article -->

			<?php endif; ?>

		</section>
		<!-- /section -->
		</div>
	</main>

	<?php get_footer(); ?>
