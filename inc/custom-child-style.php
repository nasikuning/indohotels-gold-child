<?php /*
$mainColor = Main Color
$seColor = Secondary Color
$terColor = tertiary Color

== Change Body typography ==

$font_body =  $fontsColor['font-family']
$fontsColor['font-color'] = Body typography Color
$fontHover = Hover color text

== Nav Stylish ==

 $navStyle['font-color']
 $navStyle['font-family']
 $navStyle['font-size']
 $navStyle['font-style']
 $navStyle['font-weight']

== Nav Background ==

 $navBg['background-color']
 $navBg['background-repeat']
 $navBg['background-attachment']
 $navBg['background-position']
 $navBg['background-size']
 $navBg['background-image']

== Main Background ==

 $mainBg['background-color']
 $mainBg['background-repeat']
 $mainBg['background-attachment']
 $mainBg['background-position']
 $mainBg['background-size']
 $mainBg['background-image']


== Footer Background ==

 $footerBg['background-color']
 $footerBg['background-repeat']
 $footerBg['background-attachment']
 $footerBg['background-position']
 $footerBg['background-size']
 $footerBg['background-image']

*/?>
<style id="indohotels-style" type="text/css">

	::selection {
		background:<?php echo isset($mainColor) ? $mainColor : ''; ?>;
	}

	@media(max-width: 1024px){
	  #navbar-hamburger,
		#navbar-close {
	    color: <?php echo isset($mainColor) ? $mainColor : ''; ?>;
	  }
	}

	body {
		font-family: <?php echo isset($font_body) ? $font_body : ''; ?>;
		color: <?php echo isset($fontsColor['font-color']) ? $fontsColor['font-color'] : '#000'; ?>;
	}

	.nav-background,
	.nav-no-image {
		background-color: <?php echo $navBg['background-color']; ?>;
		color: <?php echo  $navStyle['font-color']; ?>;
	}
	.navbar-nav>li>a {
		font-size: <?php echo isset($navStyle['font-size']) ? $navStyle['font-size'] : 'initial'; ?>;
		font-weight: <?php echo isset($navStyle['font-weight']) ? $navStyle['font-weight'] : ''; ?>;
	}
	.nav-background .navbar-nav>li>a {
		color: <?php echo isset($navStyle['font-color']) ? $navStyle['font-color'] : '#333'; ?>;
		font-size: <?php echo isset($navStyle['font-size']) ? $navStyle['font-size'] : 'initial'; ?>;
		font-weight: <?php echo isset($navStyle['font-weight']) ? $navStyle['font-weight'] : ''; ?>;
		text-transform: <?php echo isset($navStyle['text-transform']) ? $navStyle['text-transform'] : ''; ?>;
	}

	main[role="main"] {
		background-image: url(<?php echo $mainBg['background-image']; ?>);
		background-repeat: <?php echo $mainBg['background-repeat']; ?>;
	}

	.info-text .info-icon .icon-round {
		background: <?php echo $seColor; ?>;
	}

	.nav ul.dropdown-menu,
	.book-room,
	.home-gallery .title-gallery-home span.line-color,
	.contact-headline,
	.btn-check,
	.btn-check:active {
		background-color: <?php echo $mainColor; ?>
	}

	.btn-outline:hover,
	.btn-outline:focus,
	.btn-outline:active,
	.btn-outline.active,
	.book-room:hover,
	.book-room:hover a,
	.owl-prev .glyphicon.glyphicon-chevron-left,
	.owl-next .glyphicon.glyphicon-chevron-right {
		color: <?php echo $seColor; ?>;
	}

	.booking-section {
		background: <?php echo $mainColor; ?>
	}

	button,
	html input[type=button],
	input[type=reset],
	input[type=submit] {
		border: 1px solid <?php echo $mainColor; ?>;
		background-color: <?php echo $mainColor; ?>;
	}

	.booking .form-control,
	.booking .form-control[readonly],
	.booking .input-group-addon {
		border: 1px solid <?php echo $mainColor; ?>;
	}

	.box-gallery {
		border-top: 2px solid <?php echo $mainColor; ?>;
		border-bottom: 2px solid <?php echo $mainColor; ?>;
	}

	/* Secoundary Color */
	.btn-check:hover {
		background-color: <?php echo $seColor; ?>;
	}

	#home-featured .box-room {
		background-color: <?php echo $mainColor; ?>;
	}

	/* Font Color */
	.booking .booking-title,
	#home-featured h2 {
		color:#fff;
	}

	.box-home-grid .overlay {
		background-color: <?php echo $terColor; ?>;
	}

	/* Facilities */
	.box-text h4 {
		color: <?php echo $seColor; ?>;
	}
	.box h4 {
		background-color: <?php echo $mainColor; ?>;
	}
	.box-home-grid {
		background-color: <?php echo $terColor; ?>;
	}

	/* ROOMS */
	.room-thumb:hover,.room-thumb:active {
		background-color: <?php echo $mainColor; ?>;
		color: #fff;
	}
	.room-details h3.room-details-f-title {
		color: <?php echo $seColor; ?>;
	}
	.room-booking {
		border: 1px solid <?php echo $mainColor; ?>;
	}

	/* Footer */
	.footer-distributed {
		clear: both;
		background-color: <?php echo isset($footerBg['background-color']) ? $footerBg['background-color'] : '#6d6e71'; ?>;
		background-image: url(<?php echo isset($footerBg['background-image']) ? $footerBg['background-image'] : ''; ?>);
		background-repeat: <?php echo isset($footerBg['background-repeat']) ? $footerBg['background-repeat'] : ''; ?>;
		border-top: 2px solid <?php echo isset($mainColor) ? $mainColor : '#000'; ?>;
	}

	.footer-distributed .footer-links li:hover a, a:hover {
		color: <?php echo $fontHover; ?>;
	}
	nav .navbar-custom {
		background-color: <?php echo $terColor; ?>;
	}
	.footer-distributed {
		background-color: <?php echo $mainColor; ?>;
	}
	.footer-distributed .krs_info_widget  p a {
		color: <?php echo $seColor; ?>;
	}

	.footer-distributed .footer-icons a {
		background-color: <?php echo $seColor; ?>;
	}
	.footer-distributed .footer-icons a:hover {
		background-color: <?php echo $mainColor; ?>;
		color: #ffffff;
		border: 2px solid <?php echo $mainColor; ?>;
	}

	.footer-distributed .footer-credits {
		background-color: <?php echo $terColor; ?>;
	}

	.line {
		background: <?php echo $mainColor; ?>;
	}

	/* CALERAN ========================================================== */
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start:not(.caleran-hovered) span,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start:not(.caleran-hovered) span,
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end:not(.caleran-hovered) span,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end:not(.caleran-hovered) span,
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-selected,
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start,
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-selected,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end {
		background-color: <?php echo $seColor; ?> !important;
	}
	.caleran-container .caleran-input .caleran-header .caleran-header-end .caleran-header-end-day,
	.caleran-container .caleran-input .caleran-header .caleran-header-end .caleran-header-start-day,
	.caleran-container .caleran-input .caleran-header .caleran-header-start .caleran-header-end-day,
	.caleran-container .caleran-input .caleran-header .caleran-header-start .caleran-header-start-day,
	.caleran-container-mobile .caleran-input .caleran-header .caleran-header-end .caleran-header-end-day,
	.caleran-container-mobile .caleran-input .caleran-header .caleran-header-end .caleran-header-start-day,
	.caleran-container-mobile .caleran-input .caleran-header .caleran-header-start .caleran-header-end-day,
	.caleran-container-mobile .caleran-input .caleran-header .caleran-header-start .caleran-header-start-day {
		color: <?php echo $seColor; ?>;
	}
	.caleran-container-mobile .caleran-input .caleran-header .caleran-header-separator {
		color: <?php echo $seColor; ?>;
	}
	.caleran-container .caleran-input .caleran-calendar .caleran-days-container .caleran-today,
	.caleran-container-mobile .caleran-input .caleran-calendar .caleran-days-container .caleran-today {
		color: <?php echo $seColor; ?>;
	}
	.caleran-container-mobile .caleran-input .caleran-footer button.caleran-cancel {
		border: 1px solid <?php echo $seColor; ?> !important;
		color: <?php echo $seColor; ?> !important;
	}
	.caleran-apply-d,
	.caleran-apply {
		background: <?php echo $seColor; ?> !important;
		border: 1px solid <?php echo $seColor; ?> !important;
	}
	.caleran-container .caleran-input .caleran-calendar .caleran-days-container .caleran-day-unclick,
	.caleran-container-mobile .caleran-input .caleran-calendar .caleran-days-container .caleran-day-unclick {
		background: <?php echo $seColor; ?>;
	}

</style>
